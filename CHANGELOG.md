# Changelog
Note: version releases in the 0.x.y range may introduce breaking changes.

## 0.5.2

- patch: Update icon.

## 0.5.1

- patch: Standardising README and pipes.yml.

## 0.5.0

- minor: Switch naming conventions from task to pipes.

## 0.4.2

- patch: Move details to summary.

## 0.4.1

- patch: Restructure README.md to match user flow.

## 0.4.0

- minor: Added DEBUG option.

## 0.3.0

- minor: Fixed the version number in the pipe definition yaml and documentation.

## 0.2.0

- minor: Automatically add the --recursive option to the azcopy command if the source is a local directory.

## 0.1.0

- minor: Initial release of Bitbucket Pipelines Azure Storage deploy pipe.

